# Courtesy of:
# https://github.com/Belval/pdf2image

from pdf2image import convert_from_path
import sys, getopt

def main(argv):
    inputfile = ''
    outputfile = ''
    try:
        opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        print('pdf2jpeg.py -i <inputfile> -o <outputfile, default to inputfile_PageNumber.jpg>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('pdf2jpeg.py -i <inputfile> -o <outputfile, default to inputfile_PageNumber.jpg>')
            sys.exit()
        if opt in ("-i", "--ifile"):
            inputfile = arg
        if opt in ("-o", "--ofile"):
            outputfile = arg
    # opening from filename
    #print(inputfile)
    #print(outputfile)
    if inputfile == '':
        print('Missing inputfile')
        sys.exit()
    else:
        if outputfile == '':
            outputfile = inputfile.replace('.pdf','')
        pages = convert_from_path(inputfile, 500)
        i = 1
        for page in pages:
            page.save(outputfile+'_'+str(i)+'.jpg', 'JPEG')
            i += 1         
    
    print('Input file is: ', inputfile)
    print('Output files start from: ', outputfile+'_1.jpg')

if __name__ == "__main__":
   main(sys.argv[1:])


