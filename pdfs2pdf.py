# Courtesy of:
# http://www.blog.pythonlibrary.org/2018/04/11/splitting-and-merging-pdfs-with-python/

# pdf_merger2.py

import glob
import sys, getopt
from PyPDF2 import PdfFileMerger


def main(argv):
    inputfile = ''
    outputfile = ''
    try:
        opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        print('pdfs2pdf.py -i <inputfile_pattern> -o <output_file, default to inputfile_pattern_merge.pdf>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('pdfs2pdf.py -i <inputfile_pattern> -o <output_file, default to inputfile_pattern_merge.pdf>')
            sys.exit()
        if opt in ("-i", "--ifile"):
            inputfile = arg
        if opt in ("-o", "--ofile"):
            outputfile = arg
    # opening from filename
    #print(inputfile)
    #print(outputfile)
    if inputfile == '':
        print('Missing inputfile')
        sys.exit()
    else:
        if outputfile == '':
            outputfile = inputfile+'_merge.pdf'
        paths = glob.glob(inputfile+'_*.pdf')
        paths.sort()
        merger(outputfile, paths)
    
    print('Input file is: ', inputfile)
    print('Output file is: ', outputfile)


def merger(output_path, input_paths):
    pdf_merger = PdfFileMerger()
    file_handles = []

    for path in input_paths:
        pdf_merger.append(path)

    with open(output_path, 'wb') as fileobj:
        pdf_merger.write(fileobj)

if __name__ == '__main__':
    main(sys.argv[1:])
    #paths = glob.glob('w9_*.pdf')
    #paths.sort()
    #merger('pdf_merger2.pdf', paths)
