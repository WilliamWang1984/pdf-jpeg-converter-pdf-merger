# Courtesy of:
# 	https://pypi.org/project/img2pdf/ 
# 	https://www.tutorialspoint.com/python/python_command_line_arguments.htm

import img2pdf
import sys, getopt

def main(argv):
    inputfile = ''
    outputfile = ''
    try:
        opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    except getopt.GetoptError:
        print('jpeg2pdf.py -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('jpeg2pdf.py -i <inputfile> -o <outputfile>')
            sys.exit()
        if opt in ("-i", "--ifile"):
            inputfile = arg
        if opt in ("-o", "--ofile"):
            outputfile = arg
    # opening from filename
    #print(inputfile)
    #print(outputfile)
    if inputfile == '' or outputfile == '':
        print('Missing inputfile or outputfile')
        sys.exit()
    else:
        with open(outputfile,"wb") as f:
            f.write(img2pdf.convert(inputfile))

    print('Input file is: ', inputfile)
    print('Output file is: ', outputfile)

if __name__ == "__main__":
   main(sys.argv[1:])


